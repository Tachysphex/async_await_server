﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsyncAwait_server
{
    delegate void OnReceiveMessageHandler(object server, object client, string message);
    delegate void OnNewConnectHandler(object server, object client);
    delegate void OnClientDisconnectHandler(object server, object client);
    
    interface IServer<TServer, TClient, TClientHandle>
    {
        void Start(int port);
        void Start(string ip, int port);
        void Stop();
        void SendTo(TClientHandle clientHandle, string message);
        void SendToAll(string message);

        event OnReceiveMessageHandler MessageReceived;
        event OnNewConnectHandler NewConnect;
        event OnClientDisconnectHandler ClientDisconnected;

    }
}
