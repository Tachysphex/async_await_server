﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

namespace AsyncAwait_server
{
    class Program
    {
        static void Main(string[] args)
        {
            ServerAsyncAwait server = new ServerAsyncAwait();
            server.Start(32123);
            server.MessageReceived += new OnReceiveMessageHandler(onDataReceived);
            server.NewConnect += new OnNewConnectHandler(onCleintConnected);
            server.ClientDisconnected += new OnClientDisconnectHandler(onClientDisconnected);

            Console.ReadLine();
            server.CancelAll();
            Console.ReadLine();
            server.Stop();
            Console.ReadLine();
        }

        static void onDataReceived(object server, object client, string data)
        {
            TcpClient _client = client as TcpClient;
            Console.WriteLine("Client {0} send data: {1}", _client.Client.Handle, data);
            (server as ServerAsyncAwait).SendToAll(String.Format("Client [{0}]: {1}", _client.Client.Handle, data));
        }

        static void onCleintConnected(object server, object client)
        {
            Console.WriteLine("Client {0} has been connected", (client as TcpClient).Client.Handle);
        }

        static void onClientDisconnected(object server, object client)
        {
            Console.WriteLine("Client {0} has been disconnected", (client as TcpClient).Client.Handle);
        }
    }
}
