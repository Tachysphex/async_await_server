﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;

namespace AsyncAwait_server
{
    class WSClient
    {
        string additionalInformation = string.Empty;
        string anotherInfo = "another info";
        public TcpClient tcpClient;
        public CancellationTokenSource writeToken;
        public CancellationTokenSource readToken;
        public WSClient(TcpClient tcpClient, CancellationTokenSource writeToken, CancellationTokenSource readToken)
        {
            additionalInformation = "This is additional information about client";
            this.tcpClient = tcpClient;
            this.writeToken = writeToken;
            this.readToken = readToken;
        }
    }

    class ServerAsyncAwait : IServer<TcpListener, TcpClient, IntPtr>
    {
        #region fields

        TcpListener listener;
        CancellationTokenSource listenerCancelation = new CancellationTokenSource();
        Dictionary<IntPtr, WSClient> clientsPool = new Dictionary<IntPtr, WSClient>();
        int bufferLength = 1024;
        int port = -1;

        #endregion

        public event OnReceiveMessageHandler MessageReceived;
        public event OnNewConnectHandler NewConnect;
        public event OnClientDisconnectHandler ClientDisconnected;

        #region public

        public ServerAsyncAwait()
        {
            this.ClientDisconnected += new OnClientDisconnectHandler(clientDisconnected);
        }
        ~ServerAsyncAwait()
        {
            foreach (WSClient client in clientsPool.Values)
            {
                client.tcpClient.GetStream().Close();
                client.tcpClient.Close();
            }
            clientsPool.Clear();
            if(listener.Server.Connected)
            {
                listener.Stop();
                listener.Server.Close();
            }
        }
        public void CancelAll()
        {
            foreach(WSClient client in clientsPool.Values)
            {
                client.readToken.Cancel();
                client.writeToken.Cancel();
            }
        }
        public void Stop()
        {
            foreach (WSClient client in clientsPool.Values)
            {
                client.tcpClient.GetStream().Close();
                client.tcpClient.Close();
            }
            clientsPool.Clear();
            listener.Stop();
            listener.Server.Close();
        }
        public void Start(int port)
        {
            listener = new TcpListener(IPAddress.Any, port);
            try
            {
                listener.Start();
                this.port = port;
            }
            catch (SocketException) { return; }
            listeningAsync();
        }
        public void Start(string ip, int port)
        {
            if (!listener.Server.Connected)
                return;
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            listener = new TcpListener(ipEndPoint);
            try
            {
                listener.Start();
                this.port = port;
            }
            catch (SocketException) { return; }

        }
        public void SendToAll(string message)
        {
            sendToAll(message);
        }
        public void SendTo(IntPtr clientHandle, string message)
        {
            WSClient receiver = null;
            if(clientsPool.TryGetValue(clientHandle, out receiver))
            {
                sendAsync(receiver, message);
            }
            else
            {
                return;
            }
        }

        #endregion

        #region private

        private async void listeningAsync()
        {
            TcpClient connectedClient = null;
            try
            {
                connectedClient = await this.listener.AcceptTcpClientAsync();
            }
            catch (ObjectDisposedException) { return; }
            catch (Exception e) { Console.WriteLine("{0} : {1}", e.GetType(), e.Message); }
            WSClient adddedWSClient = new WSClient(connectedClient, new CancellationTokenSource(), new CancellationTokenSource());
            clientsPool.Add(connectedClient.Client.Handle, adddedWSClient);
            listeningAsync();
            sendToAll("NewClient" + Convert.ToString(connectedClient.Client.Handle));
            receiveAsync(adddedWSClient);
            if (NewConnect != null)
                NewConnect(this, connectedClient);
        }
        private async void receiveAsync(WSClient receiver)
        {
            if(!receiver.tcpClient.Connected)
                return;
            NetworkStream receiverStream = receiver.tcpClient.GetStream();
            if (!receiverStream.CanRead)
                return;

            string result = string.Empty;
            int bytesRead = 0;

            while (result.IndexOf("<EOF>") == -1)
            {
                byte[] buffer = new byte[bufferLength];
                int curretBytesRead = 0;

                try
                {
                    curretBytesRead = await receiverStream.ReadAsync(buffer, 0, bufferLength, receiver.readToken.Token);
                }
                catch (TaskCanceledException) { Console.WriteLine("Read task operation canceled."); return; }
                catch (OperationCanceledException) { Console.WriteLine("Read operation canceled."); return; }
                catch (ObjectDisposedException) { return; }
                catch (IOException)
                {
                    if (ClientDisconnected != null)
                        ClientDisconnected(this, receiver.tcpClient);
                    return;
                }
                catch (Exception e) { Console.WriteLine("{0} : {1}", e.GetType(), e.Message); }
                if (curretBytesRead > 0)
                {
                    bytesRead += curretBytesRead;
                    result += Encoding.UTF8.GetString(buffer, 0, curretBytesRead);
                }
                else { /* ??? */ }
            }
            receiveAsync(receiver);
            if(MessageReceived != null)
                MessageReceived(this, receiver.tcpClient, result.Replace("<EOF>", ""));
        }
        private void sendToAll(string message)
        {
            foreach(WSClient client in clientsPool.Values)
            {
                sendAsync(client, message);
            }
        }
        private async void sendAsync(WSClient receiver, byte[] data)
        {
            try
            {
                await receiver.tcpClient.GetStream().WriteAsync(data, 0, data.Length, receiver.writeToken.Token);
            }
            catch (OperationCanceledException) { Console.WriteLine("Send operation canceled."); return; }
            catch (ObjectDisposedException) { return; }
            catch (Exception e) { Console.WriteLine("{0} : {1}", e.GetType(), e.Message); }
        }
        private void sendAsync(WSClient receiver, string message)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(message + "<EOF>");
            sendAsync(receiver, buffer);
        }
        private void clientDisconnected(object server, object client)
        {
            clientsPool.Remove((client as TcpClient).Client.Handle);
            sendToAll("RemoveClient" + Convert.ToString((client as TcpClient).Client.Handle));
        }

        #endregion
    }
}
